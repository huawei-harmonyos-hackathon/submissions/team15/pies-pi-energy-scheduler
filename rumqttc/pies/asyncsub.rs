use rumqttc::{AsyncClient, Event, Incoming, MqttOptions, QoS};
use std::error::Error;
use std::time::Duration;
use tokio::{task, time};
extern crate pretty_env_logger;
extern crate serde;
use hyper::{Body, Method, Request, Uri};
use serde::Deserialize;
use std::fs;
use std::thread;

// a struct into which to decode the thing

#[derive(Deserialize)]
struct EVSEState {
    state: String,
    id: String,
    name: String,
    mqtt: String,
    http: String,
}

#[tokio::main(worker_threads = 1)]
async fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();
    // color_backtrace::install();

    let mut mqttoptions = MqttOptions::new("pies_sub", "localhost", 1883);
    mqttoptions.set_keep_alive(Duration::from_secs(5));

    let (client, mut eventloop) = AsyncClient::new(mqttoptions, 10);
    client
        .subscribe("openevse/#", QoS::AtMostOnce)
        .await
        .unwrap();

    let mut evse_state: EVSEState;
    loop {
        let notification = eventloop.poll().await.unwrap();
        //println!("Received = {:?}", notification);
        match notification {
            Event::Incoming(Incoming::Publish(msg)) => {
                // let evse_state: EVSEState = serde_json::from_slice(&msg.payload).unwrap();
                // println!("The ip address of EVSE is {:?}", evse_state.http);
                println!("{:?}", msg);

                match msg.topic {
                    _ if msg.topic == "openevse/announce/1ce0" => {
                        //println!("matched to openevse/announce/1ce0");
                        evse_state = serde_json::from_slice(&msg.payload).unwrap();
                        println!("The ip address of EVSE is {:?}", evse_state.http);
                        /*                         lazy_static! {
                            pub static ref OPENEVSE_IP: String = evse_state.http.clone();
                        } */
                    }
                    _ if msg.topic == "openevse/vehicle" => {
                        //println!("matched to openevse/vehicle");
                        if msg.payload == "0" {
                            let data = "connected";
                            fs::write("./vehicle", data).expect("Unable to write file");
                            let req_manual_override = Request::builder()
                                .method(Method::POST)
                                .uri("http://192.168.2.115/override")
                                .header("content-type", "application/json")
                                .body(Body::from(r#"{"state":"disabled"}"#))?;

                            let req_schedule = Request::builder()
                                .method(Method::POST)
                                .uri("http://192.168.2.115/schedule")
                                .header("content-type", "application/json")
                                .body(Body::from(r#"[{"id":1,"state":"active","time":"02:00:00Z","days": ["monday"]},{"id":2,"state":"disable","time":"06:00:00Z","days": ["monday"]}]"#))?;

                            let client = hyper::Client::new();

                            // POST it...
                            let resp_manual_override = client.request(req_manual_override).await?;
                            let resp_schedule = client.request(req_schedule).await?;

                            println!("Response: {}", resp_manual_override.status());
                            println!("Response: {}", resp_schedule.status());
                        } else {
                            let data = "disconnected";
                            fs::write("./vehicle", data).expect("Unable to write file");
                        }
                    }
                    _ => {}
                }
            }
            _ => {}
        }
    }
}
