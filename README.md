# PIES

The future of mobility is electric, and future of electricity is renewable. This poses unique challenges for the grid operators in terms of network stability because often there is ample supply (bright sun, windy day) but low demand or the other way round. As more and more EVs join the grid, this will become an even bigger challenge to match the electricity demand and supply.

This project aims to provide a simple and robust runtime written in Rust using the rumqtt library, which once startet on an x64 or an arm64 device such as a raspberry pi, subscribes to OpenEVSE MQTT messages from the broker running on the same device. When it detects that the charger has been plugged in schedules the charging according to the cheapest rates available. Helping the EV owners save money and grid operators stabilize their grids.

# Prerequisites

- PIES assumes  that a mqtt broker (such as mosquitto) is running on the same device i.e localhost
- It assumes that the openEVSE with WiFi is connected to the broker with the main topic ['openevse'] (https://github.com/OpenEVSE/ESP32_WiFi_V4.x)

# Compilation

```
cargo build

```
or

```
cargo build --release
```

# Running

```
cd target/debug

./pies

```

# Todos

- Energy price data from multiple energy service providers
- Configuation with a config file and/or configuration step in commandline
- Interoperability with WiFi switches such as Shelly 
- Browser control panel
- User facing application for notifications

License: Apache-2.0
